#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofEnableAlphaBlending();
    ofBackground(0);
    image.load("TripWal.png");
    width = ofGetWidth();
    height = ofGetHeight();
    shader.load("shader.vert", "shader.frag");
    poly.resize(1920 * 2);
    // grabber.setup(width, height);

    globalTime = ofGetElapsedTimef();
    for (int i = 0; i < poly.size(); i++) {

        // A Perlin Noise function that uses the current time as seed
        float xNoise = ofNoise(globalTime * 0.2f, i * 0.3f);
        float yNoise = ofNoise(-globalTime * 0.2f, i * 0.3f);

        // The noise values are normalized to the image width and height
        x = xNoise * width;
        y = yNoise * height;

        // The co-ordinates are resized to the bounds of the image
        x = ofMap(x, 0, width, 0, image.getWidth() - 1, true),
        y = ofMap(y, 0, height, 0, image.getHeight() - 1, true);

        // The radious of the circle is calculated in random order
        float radius = ofRandom(4.0, 30);
        // poly[i].setCircleResolution((int)ofRandom(30));
        // poly[i].arc(glm::vec3(x, y, 0), radius, radius, 0, 360);

        // `ofPolyline` can create a partial or closed arc with given radious,
        // resolution for the given center co-ordinates
        poly[i].arc(glm::vec3(x, y, 0), radius, radius, 0, (int)ofRandom(60, 360), (int)ofRandom(30));
        // ofMesh Mesh(poly[i].getTessellation());
        // A Mesh to get texture coordinates from the image and store it
        // correspondoiong to the arcs
        ofMesh Mesh;
        Mesh.getVertices() = poly[i].getVertices();
        // Mesh.getIndices() = poly[i].getIndices();
        Mesh.setMode(OF_PRIMITIVE_TRIANGLE_FAN);
        // Mesh.setMode(OF_PRIMITIVE_POINTS);

        for (size_t i = 1; i < Mesh.getNumVertices(); i++) {
            Mesh.addTexCoord(Mesh.getVertex(i));
        }
        meshes.push_back(Mesh);
    }
    // newPressTime = ofRandom(1920);
}

//--------------------------------------------------------------
void ofApp::update()
{
    globalTime = ofGetElapsedTimef();
    if (ofGetMousePressed(OF_MOUSE_BUTTON_LEFT)) {
        glm::vec2 newPos(ofGetMouseX(), ofGetMouseY());
        mouseGestures.derive(newPos);
        mouseGestures.average();
        speed = mouseGestures.velocity();
        acc = mouseGestures.acceleartion();
        pos = newPos;
        // cout << acc << endl;
        press = true;

        // if (ofGetFrameNum() % 60 == 0) {
        //     newAccel = ofMap(mouseGestures.avgAccel(), -15.0, 10.0, 0, ofGetWidth(), true);
        //     newSp = ofMap(mouseGestures.avgVelocity(), 0.0, 700, 0, ofGetHeight(), true);
        //     change.push_back(glm::vec2(newAccel, newSp));
        //     iterator++;
        // }
    }

    // cout << "Global: " << globalTime << " PressedTime: " << pressTime << " CurrentTime: " << newPressTime << endl;

    // grabber.update();
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSeedRandom(10);
    ofSetColor(255, 100);
    // float currentTime = ofGetElapsedTimef();
    shader.begin();
    shader.setUniformTexture("tex0", image.getTexture(), 0);

    // cout << localCurrent << endl;

    for (int i = 0; i < (int)localCurrent; i++) {
        float xNoise = ofNoise(globalTime * curSpeed, i * 0.03);
        float yNoise = ofNoise(-globalTime * curSpeed, i * 0.03);

        x = xNoise * width;
        y = yNoise * height;

        x = ofMap(x, 0, width, 0, image.getWidth() - 1, true),
        y = ofMap(y, 0, height, 0, image.getHeight() - 1, true);
        shader.setUniform1f("xLoc", x);
        shader.setUniform1f("yLoc", y);
        float radius = ofRandom(4.0, 30);
        poly[i].clear();
        // poly[i].arc(glm::vec3(x, y, 0), radius, radius, 0, 360);
        poly[i].arc(glm::vec3(x, y, 0), radius, radius, 0, (int)ofRandom(60, 360), (int)ofRandom(30));
        // meshes[i].getVertices() = poly[i].getTessellation().getVertices();
        meshes[i].getVertices() = poly[i].getVertices();
        // meshes[i].getIndices() = poly[i].getTessellation().getIndices();
        meshes[i].draw();
    }
    // ofDrawCircle(0, 0, 4);
    shader.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    // some comment to test stuff
}

//----------------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    if (button == OF_MOUSE_BUTTON_LEFT) {
        begin = std::chrono::steady_clock::now();
    }
}

//----------------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
    if (button == OF_MOUSE_BUTTON_LEFT) {
        press = false;
        done = std::chrono::steady_clock::now();
        pressTime = done - begin;
        localCurrent = ofMap(pressTime.count(), 0, 15, 10, 1920 * 2, true);
        curSpeed = ofMap(speed, -30, 30, 0.001, 0.3, true);
        // pressTime = 0;
    }
}
