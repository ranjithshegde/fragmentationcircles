#pragma once

#include "gestureFinder.h"
#include "ofMain.h"

using time_proxy = std::chrono::time_point<std::chrono::steady_clock>;
using dur_proxy = std::chrono::duration<float>;

class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);

    float position;
    ofImage image;
    // ofVideoGrabber grabber;
    int height, width;
    ofShader shader;
    float x, y;
    std::vector<ofPolyline> poly;
    std::vector<ofMesh> meshes;

    float localCurrent;
    float currentMax, currentMin, timeRad, newSp, newAccel;
    vector<float> times;
    vector<glm::vec2> change;
    glm::vec2 pos;
    float speed = 0.f;
    float acc = 0.f;
    float newAcc = 0.f;
    int iterator = 0;
    bool press = false;
    float curSpeed = 0.1, curAcc = 0.1;
    float globalTime;
    gestureFinder mouseGestures;

    ofVbo vbo;
    dur_proxy pressTime;
    time_proxy begin, done;
};
