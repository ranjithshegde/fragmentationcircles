#include "ofApp.h"
#include "ofMain.h"

//========================================================================
int main()
{
#ifdef OF_TARGET_OPENGLES
    ofGLESWindowSettings settings;
    settings.glesVersion = 3;
#else
    ofGLWindowSettings settings;
    settings.setGLVersion(4, 6);
    settings.setSize(1920, 1080);
#endif

    ofCreateWindow(settings);
    ofRunApp(new ofApp());
}
