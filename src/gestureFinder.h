#pragma once
#include "ofMain.h"

class gestureFinder {
public:
    gestureFinder();
    // gestureFinder(gestureFinder&&) = default;
    // gestureFinder(const gestureFinder&) = default;
    // gestureFinder& operator=(gestureFinder&&) = default;
    // gestureFinder& operator=(const gestureFinder&) = default;
    ~gestureFinder();

    struct derivatives {
        float firstDt, secondDt;
        float oldFirst, oldSecond;
        unsigned int period, count;
        double averageDt, averageSecondDt;
    };

    void derive(float value);
    void derive(glm::vec2 value);

    void average();

    float velocity();
    float acceleartion();
    float oldValue();
    float oldDt();

    inline void setPeriod(unsigned int period)
    {
        m_Deriv.period = period;
    }

    double avgVelocity();
    double avgAccel();

private:
    derivatives m_Deriv;
    double tempFirst, tempSecond;
};
