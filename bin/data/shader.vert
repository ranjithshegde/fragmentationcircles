#version 460

uniform mat4 modelViewProjectionMatrix;
in vec4 position;
in vec2 texcoord;
out vec2 newTex;
out vec2 locAsTex;

void main()
{
    gl_Position = modelViewProjectionMatrix * position;
    newTex = texcoord;
    locAsTex = vec2(position.x, position.y);
}
