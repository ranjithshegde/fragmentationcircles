#version 460

uniform sampler2DRect tex0;
uniform float xLoc;
uniform float yLoc;
uniform float rad;
in vec2 newTex;
in vec2 locAsTex;
out vec4 outputColor;

void main()
{
    // float newPosX = distance(xLoc, locAsTex.x);
    // float newPosY = distance(yLoc, locAsTex.y);
    // outputColor = texture(tex0, vec2(xLoc + newPosX,yLoc + newPosY));

    // outputColor = texture(tex0, newTex);
    outputColor = texture(tex0, locAsTex);

    // outputColor = texture(tex0, vec2(xLoc + newTex.x, yLoc + newTex.y));
    outputColor.a = 0.85;
}
