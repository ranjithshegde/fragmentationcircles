# Image fragment animation

This is a small example of dividing an image into several fragments and
dynamically rearranging them interactively.

In function `ofApp::setup()` a Mesh is created to store image fragments. The
fragment are formed using `ofPolyline` to create a dynamically sized circle with
variable resolution. The resolution allows for the circles to be approximation
of polygons of different variety.

```cpp
for (int i = 0; i < poly.size(); i++) {

    // A Perlin Noise function that uses the current time as seed
    float xNoise = ofNoise(globalTime * 0.2f, i * 0.3f);
    float yNoise = ofNoise(-globalTime * 0.2f, i * 0.3f);

    // The noise values are normalized to the image width and height
    x = xNoise * width;
    y = yNoise * height;

    // The co-ordinates are resized to the bounds of the image
    x = ofMap(x, 0, width, 0, image.getWidth() - 1, true),
    y = ofMap(y, 0, height, 0, image.getHeight() - 1, true);

    // The radius of the circle is calculated in random order
    float radius = ofRandom(4.0, 30);

    // `ofPolyline` can create a partial or closed arc with given radious,
    // resolution for the given center co-ordinates
    poly[i].arc(glm::vec3(x, y, 0), radius, radius, 0, (int)ofRandom(60, 360), (int)ofRandom(30));
    // ofMesh Mesh(poly[i].getTessellation());
    // A Mesh to get texture coordinates from the image and store it
    // corresponding to the arcs
    ofMesh Mesh;
    Mesh.getVertices() = poly[i].getVertices();
    Mesh.setMode(OF_PRIMITIVE_TRIANGLE_FAN);

    for (size_t i = 1; i < Mesh.getNumVertices(); i++) {
        Mesh.addTexCoord(Mesh.getVertex(i));
    }
    meshes.push_back(Mesh);
}
```

Using the Wacom tablet Gesture tracking algorithm defined in [this project](https://gitlab.com/ranjithshegde/gesturederivatives), the position and viewport of each of these crude circles are dynamically set in a callback function `ofApp::update()`. This callback function is bound to the graphic card with `VSync`

These newly calculated values are sent to the fragment shader.

The fragment shader offers two choices.

1. Keep the texture in place and move the individual viewports. Where the circle
   moves at any given point, it just displays the texture values at that
   location.

2. Attain the texture values at init position and move the texture along with
   the circles. This way the all the fragments of the images themselves appear
   to be moving

```cpp
#version 460

uniform sampler2DRect tex0;
uniform float xLoc;
uniform float yLoc;
uniform float rad;
in vec2 newTex;
in vec2 locAsTex;
out vec4 outputColor;

void main()
{
    // Use this function to change texture location for animation.
    // outputColor = texture(tex0, newTex);
    // Use this function to change viewport with static texture for animation
    outputColor = texture(tex0, locAsTex);

    outputColor.a = 0.85;
}
```
